@c -*-texinfo-*-
@c This is part of the GNU G-Golf Reference Manual.
@c Copyright (C) 2022 - 2024 Free Software Foundation, Inc.
@c See the file g-golf.texi for copying conditions.


@node Utils Corner
@subsection Utils Corner

Utils Corner. Some utilities.


@subheading Syntax

@indentedblock
@table @code
@item @ref{scm->g-type}
@item @ref{allocate-c-struct}
@end table
@end indentedblock


@subheading Description

Welcome to the G-Golf Utils Corner.


@subheading Syntax


@anchor{scm->g-type}
@deffn Procedure scm->g-type value

Returns a GType.

Obtains and returns the GType for @var{value}, which may be a number
(then assumed to be a valid GType), a string, a symbol (a
@ref{%g-type-fundamental-types} member) or a <gobject-class>.
@end deffn


@anchor{allocate-c-struct}
@deffn Syntax allocate-c-struct name . fields

Returns a (or more) pointer(s).

This syntax takes the @var{name} of a GI upstream library C
struct@footnote{More specifically, an unquoted scheme representation
name of a GI upstream library C struct.} and returns a pointer to a
newly - scheme allocated, zero initialized - memory block.

When @var{fields} is not null?, it returns additional value(s), one for
each specified field name, a pointer to the field in the C struct.

Here is an example, an excerpt form the peg-solitaire.scm example,
distributed with G-Golf. The example shows how to obtain a pointer to
newly allocated block for a @code{GskRoundedRect}, as well as a pointer
to its @code{bounds} field:

@example
(receive (outline outline:bounds)
    (allocate-c-struct gsk-rounded-rect bounds)
  ...
  (push-rounded-clip snapshot outline)
  (append-color snapshot
                '(0.61 0.1 0.47 1.0)
                 outline:bounds)
  ...)
@end example  
@end deffn
