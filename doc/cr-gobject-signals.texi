@c -*-texinfo-*-

@c This is part of the GNU G-Golf Reference Manual.
@c Copyright (C) 2019 - 2023 Free Software Foundation, Inc.
@c See the file g-golf.texi for copying conditions.


@node Signals
@subsection Signals

G-Golf GObject Signals low level API.@*
Signals — A means for customization of object behaviour and a general
purpose notification mechanism


@subheading Procedures

@indentedblock
@table @code
@item @ref{g-signal-newv}
@item @ref{g-signal-query}
@item @ref{g-signal-lookup}
@item @ref{g-signal-list-ids}
@item @ref{g-signal-emitv}
@item @ref{g-signal-connect-closure-by-id}
@item @ref{g-signal-handler-disconnect}
@item @ref{g-signal-parse-name}
@end table
@end indentedblock


@subheading Types and Values

@indentedblock
@table @code
@item @ref{%g-signal-flags}
@end table
@end indentedblock


@subheading Description

The basic concept of the signal system is that of the emission of a
signal. Signals are introduced per-type and are identified through
strings. Signals introduced for a parent type are available in derived
types as well, so basically they are a per-type facility that is
inherited.

Please read the @uref{@value{UGOBJECT-Signals}, Signals} section from
the GObject reference manual for a complete description.


@subheading Procedures


@anchor{g-signal-newv}
@deffn Procedure g-signal-newv name iface-type flags class-closure @
         accumulator accu-data c-marshaller return-type @
         n-param param-types

Returns the signal id.

Creates a new signal. The arguments are:

@indentedblock
@table @var

@item name
The name for the signal.

@item iface-type
The type this signal pertains to. It will also pertain to types which
are derived from this type.

@item flags
A list of @ref{%g-signal-flags}, specifying detail of when the default
handler is to be invoked. It should at least specify @code{run-first}
or @code{run-last}.

@item class-closure
The closure to invoke on signal emission, may be #f.

@item accumulator
The accumulator for this signal; may be #f.

@item accu-data
User data for the accumulator.

@item c-marshaller
The function to translate arrays of parameter values to signal emissions
into C language callback invocations or #f.

@item return-type
The GType of the signal returned value. The caller may obtain the GType,
given a scheme object (or @code{'none} for a signal without a return
value), by calling @ref{scm->g-type}.

@item n-param
The length of @var{param-types}.

@item param-types
An list of types, one for each parameter (may be @code{'()} if
@var{n-param} is zero).
@end table
@end indentedblock

@end deffn


@anchor{g-signal-query}
@deffn Procedure g-signal-query id

Returns a list.

Obtains and returns a list composed of the signal id, name,
interface-type@footnote{Within this context, the interface-type is the
@code{GType} of the @code{GObject} subclass the signal is @samp{attached
to} - knowing that signals are inhereted.}, flags, return-type, number
of arguments and their types. For example@footnote{At least one
@code{GObject} subclass instance must have been created prior to attempt
to query any of its class signal(s).}:

@lisp
,use (g-golf)
(gi-import "Clutter")

(make <clutter-actor>)
@result{} $2 = #<<clutter-actor> 565218c88a80>

(!g-type (class-of $2))
@result{} $3 = 94910597864000

(g-signal-list-ids $3)
@result{} $4 = (5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30)

(g-signal-query 20)
@result{} $5 = (20 "enter-event" 94910597864000 (run-last) boolean 1 (boxed))
@end lisp

As you may have noticed, the signal query argument(s) list does not
include the instance (and its type) upon which the signal is called, but
both at C level and within the context of @code{GClosure}, callbacks
must assume that the instance upon which a signal is called is always
the first argument of the callback.
@end deffn


@anchor{g-signal-lookup}
@deffn Procedure g-signal-lookup name g-type

Returns an integer.

Obtains and returns the signal's identifying integer, given the
@var{name} of the signal and the object @var{g-type} it connects to. If
a signal identifier can't be find for the given @var{name} and
@var{g-type}, an exception is raised.
@end deffn


@anchor{g-signal-list-ids}
@deffn Procedure g-signal-list-ids g-type

Returns a list of integers.

Obtains and returns the list of signal's identifying integers for
@var{g-type} (Note that at least one @var{g-type} instance must have
been created prior to attempt to list or query signal's identifying
integers for a given @var{g-type}).
@end deffn


@anchor{g-signal-emitv}
@deffn Procedure g-signal-emitv params id detail return-value

Returns nothing.

Emits a signal. Signal emission is done synchronously. The method will
only return control after all handlers are called or signal emission was
stopped.

Note that @ref{g-signal-emitv} doesn't change @var{return-value} if no
handlers are connected.

The @var{params} points to the argument list for the signal
emission. The first element in the array is a GValue for the instance
the signal is being emitted on. The rest are any arguments to be passed
to the signal. The @var{id} is the signal id, @var{detail} the detail (a
g-quark and @var{return-value} the location to store the return value of
the signal emission (it must be provided if the specified signal returns
a value, but may be ignored otherwise).
@end deffn


@anchor{g-signal-connect-closure-by-id}
@deffn Procedure g-signal-connect-closure-by-id instance id detail closure after

Returns the handler ID (always greater than 0 for successful
connections).

Connects a closure to a signal for a particular object.

If @var{closure} is a floating reference (see @ref{g-closure-sink}),
this function takes ownership of closure.

The @var{instance} is the instance to connect to, the @var{id} the id of
the signal, @var{detail} the detail (a g-quark). @var{closure} the
closure to connect, @var{after} (a boolean) whether the handler should
be called before or after the default handler of the signal.
@end deffn


@anchor{g-signal-handler-disconnect}
@deffn Procedure g-signal-handler-disconnect instance handler-id

Returns nothing.

Disconnects a handler from an instance so it will not be called during
any future or currently ongoing emissions of the signal it has been
connected to. The @var{handler-id} becomes invalid and may be reused.

The @var{handler-id} has to be a valid signal handler id, connected to a
signal of instance .
@end deffn


@anchor{g-signal-parse-name}
@deffn Procedure g-signal-parse-name detailed-signal g-type @
                                     [force-detail-quark #t]

Returns two integer values.

Obtains and returns the signal-id and a detail corresponding to
@var{detailed-signal} for @var{g-type}. The @var{detailed-signal} can be
passed as a symbol or a string. When @var{force-detail-quark} is
@code{#t} it forces the creation of a @code{GQuark} for the detail.

If the signal name could not successfully be parsed, it raises an
exception.
@end deffn


@subheading Types and Values


@anchor{%g-signal-flags}
@defivar <gi-enum> %g-signal-flags

The signal flags are used to specify a signal's behaviour, the overall
signal description outlines how especially the RUN flags control the
stages of a signal emission.

An instance of @code{<gi-enum>}, who's members are the scheme
representation of the @code{GSignalFlags}:

@indentedblock
@emph{g-name}: GSignalFlags  @*
@emph{name}: g-signal-flags  @*
@emph{enum-set}:
@indentedblock
@table @code

@item run-first
Invoke the object method handler in the first emission stage.

@item run-last
Invoke the object method handler in the third emission stage.

@item run-cleanup
Invoke the object method handler in the last emission stage.

@item no-recurse
Signals being emitted for an object while currently being in emission
for this very object will not be emitted recursively, but instead cause
the first emission to be restarted.

@item detailed
This signal supports "::detail" appendices to the signal name upon
handler connections and emissions.

@item action
Action signals are signals that may freely be emitted on alive objects
from user code via @code{g-signal-emit} and friends, without the need of
being embedded into extra code that performs pre or post emission
adjustments on the object. They can also be thought of as object methods
which can be called generically by third-party code.

@item no-hooks
No emissions hooks are supported for this signal.

@item must-collect
Varargs signal emission will always collect the arguments, even if there
are no signal handlers connected. Since 2.30.

@item deprecated
The signal is deprecated and will be removed in a future version. A
warning will be generated if it is connected while running with
@code{G_ENABLE_DIAGNOSTIC=1}. Since 2.32.

@end table
@end indentedblock
@end indentedblock
@end defivar
