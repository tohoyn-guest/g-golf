@c -*-texinfo-*-
@c This is part of the GNU G-Golf Reference Manual.
@c Copyright (C) 2016 - 2024 Free Software Foundation, Inc.
@c See the file g-golf.texi for copying conditions.


@node Using G-Golf
@c @chapter Using G-Golf
@unnumbered II. Using G-Golf

@c G-Golf Reference Manual still is a mock-up: any help is more then
@c welcome to improve this situation, thanks!


@menu
* Before you start::
* Getting Started with G-Golf::
* Working with GNOME::
* G-Golf Valley::
* Support Canyon::
@end menu


@include ug-bys.texi
@include ug-gswg.texi
@include ug-wwg.texi
@include ug-ggv.texi
@include ug-sc.texi
